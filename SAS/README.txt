%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Authors : Pierre Bunouf, Geert Molenberghs, Herbert Thijs, Jean-Marie Grouin  %
%  Contact : pierre.bunouf@pierre-fabre.com                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The program is launched from SAS, and the R script is called and executed
automatically from wihin the SAS program. To do so, we use new features in SAS
PROC IML that enable to call R functions from within the IML procedure. This
automatic link is available from version 9.3 of SAS under Windows and Linux
onwards. Nevertheless, if another operating system is used and/or if SAS PROC
IML version 9.3 or later is not available, instructions are provided in the JSS
manuscript `A SAS program combining R functionalities to implement
pattern-mixture models', Section 3.1, to execute the program anyway.

To run the case study of the JSS manuscript, the user must upload the three
files `PMM.sas, `PMM.R', and `Data.sas7bdat' in the same working directory and
follow the instructions mentioned in the JSS manuscript. The environment
requirements are detailed in Section 3.2, whereas information to initiate the
program is provided in Section 4. 

The program produces inferences based on multiple imputation. As any Monte Carlo
method, fluctuation of results is inevitable if a limited number of imputations
is used. With the default parametrization, the treatment effect estimated in the
case study is expected to vary by a certain amount as it is based on 500
imputations. Conversely, Table 2 in the JSS manuscript shows results which are
expected to exhibit very small fluctuatuation, but they are based on 10,000
imputations. It is important to note that with 10,000 imputations, the program
takes 3 hours to produce results against 8 minutes with 500 imputations, using a
computer with I5 processor. 

We now provide practical information to generate the results shown in Table 2 in
the JSS manuscript, which are based on 10,000 imputations. To gain time, a
multi-step procedure has been introduced in the algorithm to enable multiple
imputation by batch. The reason is that the time of execution by imputation
increases as the number of imputations increases because of cumbersome data
handling. Table 1 here-below provides times of execution obtained using a
computer with I5 processor and standard environment for running SAS and R.

Table 1. Times of execution for a batch of imputations and by imputation
according to the number of imputations by batch.

# of imputations	Total time for a batch    	Time by imputation
50 			38 sec 				0.8 sec 
100			1 min 38 sec			1 sec
250 			6 min 32 sec			1.5 sec
500 			21 min 58 sec 			2 sec 


Table 1 here-above shows that the time of execution by imputation for a batch of
500 imputations is twice greater than for a batch of 100 imputations. So, as
already specified in Section 3.1 of JSS manuscript, it is twice less time
consuming to run 5 batches of 100 imputations than directly 500 imputations. 

For 10,000 imputations, we recommend 100 batches of 100 imputations. We provide
here-below the corresponding SAS code and output for the default MI
parametrization ("Restriction=NFMV-CC and Delta=2"). It is important to note
that, despite the large number of imputations, any new execution of the program
will not reproduce exactly the same results as those shown here-below, but one
can expect an accuracy level of 2 decimals to the treatment-effect estimate and
3 decimals to the significance level.  

__________________________________________________________________________________
Lines 47-52 of the SAS program, enter:

* Indicate parameters of the MI procedure here-below;
%let nBatches=100;
%let nImputations=100;
%let Restriction=NFMV-CC;
%let Delta=2;
%let Rounding=1;

__________________________________________________________________________________
SAS output

                              Pattern-mixture model under NFMV-CC identifying restriction and location parameter Delta=2                                  
                                                           Pooled analysis of fixed effects                                                               
                                                                                                                                                          
                                                               The MIANALYZE Procedure                                                                    
                                                                                                                                                          
                                                                  Model Information                                                                       
                                                                                                                                                          
                                                     PARMS Data Set            WORK.SOLUTIONTOTAL                                                         
                                                     COVB Data Set             WORK.COVBTOTAL                                                             
                                                     Number of Imputations     10000                                                                      
                                                                                                                                                          
                                                                                                                                                          
                                                                Variance Information                                                                      
                                                                                                                                                          
                                                                                           Relative       Fraction                                        
                                 -----------------Variance-----------------                Increase        Missing       Relative                         
                    Parameter         Between         Within          Total       DF    in Variance    Information     Efficiency                         
                                                                                                                                                          
                    Cov1          0.000056529       0.001277       0.001334   5.57E6       0.044259       0.042384       0.999996                         
                    Int1             0.171651       4.448243       4.619910   7.24E6       0.038592       0.037159       0.999996                         
                    Int2             0.172468       5.025871       5.198356   9.08E6       0.034319       0.033181       0.999997                         
                    Int3             0.192179       5.578109       5.770307   9.01E6       0.034456       0.033308       0.999997                         
                    Int4             0.383259       6.290671       6.673968   3.03E6       0.060931       0.057432       0.999994                         
                    Group1        0.000013173       1.159958       1.159971   775E11    0.000011357    0.000011357       1.000000                         
                    Group2           0.040990       2.336030       2.377024   3.36E7       0.017549       0.017246       0.999998                         
                    Group3           0.164841       3.460407       3.625265   4.84E6       0.047641       0.045475       0.999995                         
                    Group4           0.775276       4.911209       5.686563   537844       0.157874       0.136352       0.999986                         
                                                                                                                                                          
                                                                                                                                                          
                                                                 Parameter Estimates                                                                      
                                                                                                                                                          
                                                                                                                                  t for H0:               
Parameter        Estimate      Std Error    95% Confidence Limits        DF        Minimum        Maximum         Theta0   Parameter=Theta0   Pr > |t|    
                                                                                                                                                          
Cov1             0.897439       0.036522       0.8259      0.96902   5.57E6       0.870072       0.926932              0              24.57     <.0001    
Int1             4.547193       2.149398       0.3344      8.75994   7.24E6       2.922042       6.055245              0               2.12     0.0344    
Int2             3.593581       2.279990      -0.8751      8.06228   9.08E6       1.893999       5.055412              0               1.58     0.1150    
Int3            -0.170673       2.402146      -4.8788      4.53745   9.01E6      -1.826254       1.432055              0              -0.07     0.9434    
Int4            -5.303332       2.583403     -10.3667     -0.23995   3.03E6      -8.376985      -3.024364              0              -2.05     0.0401    
Group1          -2.674891       1.077020      -4.7858     -0.56397   775E11      -2.688102      -2.660654              0              -2.48     0.0130    
Group2          -4.001584       1.541760      -7.0234     -0.97979   3.36E7      -4.859841      -3.265619              0              -2.60     0.0094    
Group3          -3.005822       1.904013      -6.7376      0.72598   4.84E6      -4.593903      -1.398870              0              -1.58     0.1144    
Group4          -4.443857       2.384651      -9.1177      0.22998   537844      -7.527718      -0.398590              0              -1.86     0.0624    

__________________________________________________________________________________


Then, in the file "PROC_MIXED_LST.txt", we  provide the output obtained from SAS
PROC MIXED for parameter estimation before calling R. This may help readers
without access to SAS to reproduce the corresponding R part. In this file, we
have also edited the 3 data files exported from SAS to R, which are: 

1.	Estimates2R, which contains the pattern parameter estimates, 
2.	Outcome2R, which contains the outcome values and derived variables, 
3.	MIparameters2r, which contains the parameters for imputation.

Further description of the three data files is available in Section 5.1 in the
manuscript.

To end, we provide an additional file containing the R code shown in Section
5.2. This information is aimed at describing the method and the different steps
of the programming in the imputation stage. The file "Section52.R" contains
the R script and its output is in "Section52.out".

The intermediate results can obtained by:

1.	Running the program,
2.	Executing the R script contained in the file "Section52.R".
