/*******************************************************************************************

This program analyzes incomplete continuous longitudinal outcome using pattern-mixture 
models under CCMV, ACMV, NCMV and NFMV identifying restrictions.

In the SAS code here-below, user must

1. indicate the paths to access files in the SAS macro variable:
- "&Path2WorkDir" : path to access the working directory.

User must also indicate paths to access files at the top of the R file PMM.r in the variables:
- "Path2WorkDir"  : path to access the working directory,
- "Path2SASexe"   : path to access the file SAS.exe.

2. define the imputation and the analysis model parameters in the SAS macro variable "&Covariates" and 
the design matrix columns in the data step "DefineColumns".

3. indicate parameters of the MI procedure in the SAS macro variables:
- "&nBatches" 	  : number of batches of imputations (positive integer), 
- "&nImputations" : number of imputations by batch (positive integer), 
- "&Restriction"  : identifying restriction among CCMV, ACMV, NCMV, NFMV-CC, and NFMV-NC,
- "&Delta" 	  : location parameter for NFMV identifying restriction,
- "&Rounding" 	  : rounding of imputed values (positive integer).

********************************************************************************************/

* Define path to access the working directory here-below;
%let Path2WorkDir=/folders/myfolders;

* Define the imputation and the analysis model parameters and the design matrix columns here-below;
%let covariates = cov1 int1 int2 int3 int4 group1 group2 group3 group4;
libname DataFile "&Path2WorkDir";
data DefineColumns;
	set DataFile.Data;
	Int1 = 0; Int2 = 0; Int3 = 0; Int4 = 0;
	Group1 = 0; Group2 = 0; Group3 = 0; Group4 = 0; 
	if Rep = 1 then Int1 = 1;
	if Rep = 2 then Int2 = 1;
	if Rep = 3 then Int3 = 1;
	if Rep = 4 then Int4 = 1;
	if Rep = 1 and Group = 2 then Group1 = 1;
	if Rep = 2 and Group = 2 then Group2 = 1;
	if Rep = 3 and Group = 2 then Group3 = 1;
	if Rep = 4 and Group = 2 then Group4 = 1;
run;

* Indicate parameters of the MI procedure here-below;
%let nBatches=5;
%let nImputations=100;
%let Restriction=NFMV-CC;
%let Delta=2;
%let Rounding=1;


/*******************************************************************************************

The SAS code here-below up to the SAS/IML call creates three SAS data sets that will be 
exported to R for multiple imputation :

- "MIparameters2R" : contains parameter values for multiple imputation procedure, 
- "Outcome2R" : contains outcome values and derived variables,
- "Estimates2R" : contains pattern parameter estimates.

********************************************************************************************/

proc options option=RLANG; run;
options  nonumber nodate;
title;
%macro TitleOne;
	%if "%cmpres(&Restriction)"="CCMV" or "%cmpres(&Restriction)"="NCMV" or "%cmpres(&Restriction)"="ACMV" %then %do;
		Title1 "Pattern-mixture model under &Restriction identifying restriction";
	%end;
	%else %do;
		Title1 "Pattern-mixture model under &Restriction identifying restriction and location parameter Delta=&Delta";
	%end;
%mend;
%TitleOne;

proc sort data=DefineColumns out=DataNormalized1; by Subject descending Rep; run; 
data DataNormalized2;
	retain FlagDrop;
	set DataNormalized1;
	by Subject;
	MissDrop = 0 ; MissInter = 0;
	if Outcome = . and first.Subject then do; MissDrop = 1; FlagDrop = 1; end;
	if Outcome = . and not(first.Subject) and FlagDrop=1 then MissDrop = 1; 
	if Outcome = . and not(first.Subject) and MissDrop = 0 then MissInter = 1; 
	if Outcome ne . then FlagDrop = 0; 
	drop FlagDrop;
run;


proc sort data=DataNormalized2 out=DataNormalized3; by Subject Rep; run; 

proc summary data=DataNormalized3;
	var MissDrop;
	by Subject;
	output out=Pattern n=N sum=NminusPattern ;
run;

data DataFile.Outcome2R;
	retain Subject Rep Group Outcome MissDrop MissInter Pattern nValues &Covariates;
	merge DataNormalized3 Pattern;
	by Subject;
	Pattern = N-NminusPattern;
	nValues = Pattern;
	keep Subject Rep Group Outcome MissDrop MissInter Pattern nValues &Covariates;
run;

/************************************************************************************************************

(1) estimate pattern parameters,

************************************************************************************************************/

proc Sort data=DataFile.Outcome2R out=OutcomeSort; by Pattern; run;
proc Mixed data=OutcomeSort method=ml noclprint noitprint asycov covtest;
	title2 "Parameter estimation per pattern";
	class Subject Rep;
	model Outcome = %str(&Covariates) / noint s covb;
	ods output solutionf = Solution;
	ods output covb = CovB;
	ods output covparms = CovParms;
	ods output asycov = AsyCov;
	repeated Rep / subject=Subject type=UN r;
	by Pattern;
run;

proc sort data=Solution; by Pattern; run;
data Solution; 
	format Parameter $8.;
	set Solution; 
	by pattern;
	Parameter = "Beta"; 
	Retain Pattern;
	if first.Pattern 	then Number = 1;
						else Number + 1;
	output; 
	keep Parameter Pattern Number Estimate; 
run;

data CovB; set CovB; Obs = _N_; run;
proc Sort data = CovB; by Obs; run;
proc Transpose data = CovB out = CovBT1; by Obs; run;
data CovBT2; set CovBT1; if _NAME_ ne "Row"; rename Col1 = Estimate; run;
data CovBT3; 
	format Parameter $8.;
	set CovBT2; 
	if _NAME_ = "Pattern" then PatternDEl = Estimate; 
	Pattern=lag(PatternDel);
	retain Flag;
	if Pattern ne . then Flag = Pattern;
				 	else Pattern = Flag;
	if _NAME_ = "Pattern" then delete;
	Parameter = "BetaCov"; 
run;
proc Sort data = CovBT3; by Pattern; run;
data CovBT; 
	set CovBT3; 
	by Pattern;
	Retain Pattern;
	if first.Pattern 	then Number = 1;
						else Number + 1;
	output; 
	keep Parameter Pattern Number Estimate; 
run;

proc Sort data = CovParms; by Pattern; run;
data CovParms;
	format Parameter $8.;
	set CovParms;
	by pattern;
	Parameter = "Sigma";
	if Estimate = 5.25E-8 or STDErr=0 then Estimate = 0;
	Row = substr(Compress(CovParm),4,1)*1;
	Col = substr(Compress(CovParm),6,1)*1;
	keep Parameter Pattern Estimate Row Col; 
run;
proc Sort data = CovParms; by Pattern Col Row; run;
data CovParms;
	set CovParms;
	by Pattern;
	if first.Pattern 	then Number = 1;
						else Number + 1;
	output; 
	drop Row Col;
run;

data AsyCov; set AsyCov; Obs =_N_; run;
proc Sort data = AsyCov; by Obs; run;
proc Transpose data = AsyCov out = AsyCovT1; by Obs; run;
data AsyCovT2; set AsyCovT1; if _NAME_ ne "Row"; rename Col1 = Estimate; run;
data AsyCovT3; 
	format Parameter $8.;
	set AsyCovT2; 
	if _NAME_ = "Pattern" then PatternDEl = Estimate; 
	Pattern = lag(PatternDel);
	retain Flag;
	if Pattern ne . then Flag = Pattern;
				 	else Pattern = Flag;
	if _NAME_ = "Pattern" then delete;
	Parameter = "SigmaCov"; 
run;
proc Sort data=AsyCovT3; by Pattern; run;
data AsyCovT; 
	set AsyCovT3; 
	by Pattern;
	Retain Pattern;
	if first.Pattern 	then Number = 1;
						else Number + 1;
	output; 
	if Estimate = 5.25E-8 then Estimate = 0;
	keep Parameter Pattern Number Estimate; 
run;

Data DataFile.Estimates2R;	
	retain Parameter Pattern Number Estimate;
	set Solution CovBT CovParms AsyCovT;
	rename Estimate = Value;
run;
Data DataFile.MIparameters2R;
	Path2WorkDir = "&Path2WorkDir";
	nImputations = &nImputations;
	Restriction  = "&Restriction";
	Delta		 = &Delta;
run;

%macro Duplicate;

	%do imput=1 %to &nImputations; 

		%if &imput=1 	%then %do; data OutcomeComplete; set DataFile.Outcome2R; Imputation=1; run; %end;   
			   			%else %do; data OutcomeComplete; set OutcomeComplete DataFile.Outcome2R; if Imputation=. then Imputation=&imput; run; %end;

	%end;

%mend;

%macro Imputation;

	%do batch=1 %to &nBatches; 

		/************************************************************************************************************

		(2) impute missing outcome values using R,

		************************************************************************************************************/

		proc iml;

			%include "&Path2WorkDir/PMM.r";

		quit;
			
			%include "&Path2WorkDir/OutcomeImput2SAS.sas";

		proc sort data=OutcomeComplete; 	by imputation subject rep; run;
		proc sort data=OutcomeImput; 		by imputation subject rep; run;

		data OutcomeForPooledAnalysis;
			merge OutcomeComplete OutcomeImput;
			format outcome 8.&Rounding;
			by imputation subject rep;
		run;

		/********************************************************************************

		(3) analyze the complete datasets by imputation,

		********************************************************************************/

		ods listing close;
		proc Mixed data = OutcomeForPooledAnalysis method=ml noclprint noitprint covtest;
			title2 "Longitudinal outcome analysis by imputation";
			class subject rep;
			model outcome = %str(&Covariates) / noint s covb;
			ods output solutionf = Solution;
			ods output covb = covb;
			repeated rep / subject=subject type=UN r;
			by imputation;
		run;
		ods listing;

		data Solution; 	set Solution; 	_Imputation_=Imputation; drop Imputation; run;
		data CovB; 		set CovB; 		_Imputation_=Imputation; drop Imputation; run;

		%if &batch=1 	%then %do;	
					data SolutionTotal; set Solution; 	run;
					data CovBTotal; 	set CovB; 		run; 
							 %end;   

			   			%else %do; 	
					data SolutionTotal; set SolutionTotal Solution; run; 
					data CovBTotal; 	set CovBTotal CovB; 		run; 
							 %end;

	%end;

%mend;

%Duplicate

%Imputation

/********************************************************************************

(4) combine results by imputation for pooled inference. 

********************************************************************************/

proc MIanalyze parms=SolutionTotal covb(effectvar=rowcol)=CovBTotal;
title2 "Pooled analysis of fixed effects";
modeleffects &Covariates ;
run;
