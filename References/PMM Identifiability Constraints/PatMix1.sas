
*******************************************************************************************************
** PatMix1 macro for basic pattern mixture modeling of data missing not at random (MNAR)             **
*******************************************************************************************************;



***Program PatMix1.SAS;


*********************************************************************************************************************;
******  PART 1:    SPECIFY SAS OPTIONS                                                                           ****;
*********************************************************************************************************************;



OPTIONS LS=131 PS=46 NOCENTER;
* OPTIONS MPRINT SYMBOLGEN MLOGIC;
TITLE;






***********************************************************************************************************************************;
******  PART 2:    PREPARE DATA                                                                                                ****;
***********************************************************************************************************************************;




*******************************************************************************************************
** Place Code to get data in proper format for later analysis.  Dataset should contain 1 observation **
** per patient per visit, and include all required model variables. If a schedule visit has missing  **
*  response variabl, then a missing value is used for that visit.                                    **
*                                                                                                    **
*  Additionally, the following variables will need to be created accordingly:                        **
**                                                                                                   **
** Treatment = A numerical variable (1,2,3,...) representing different treatment groups.             **
** Visit = Visit Value                                                                               **
** Base = Baseline value included in the model                                                       **
** Subjid = Patient number                                                                           **
**                                                                                                   **
** Also need to derive variable dgroup = dropout group.  This is also a numerical variable that      **
** captures the highest visit value included within a dropout group (if each visit is a dropout group**
** then this is simply the same as the last visit for each patient).  In cases where multiple visits **
** are included in one dropout group, the group is identified by the highest visit number.           **
**                                                                                                   **
** The dataset name with all this information is named "all" to appropriately                        **
** integrate with the macros                                                                         **
*******************************************************************************************************;


%LET analyset=datalib.&DataName;
%let modvars = inv subjid treatment base; *<-- Subject ID and Variables in original model other than visit;





****************************
** Study Specific Formats **
** and Output Titles      **
****************************;


data _temp;
  set  &analyset;
  if change>.;
 subjid=patient;
 base=basval;
 inv=poolinv; 

 if THERAPY = "PLACEBO" then treatment=2;
 if THERAPY = "DRUG" then treatment=1;
 
if treatment ne . and change ne . and base ne .;

keep inv subjid treatment visit base change;
run;

/*
proc print n;
run;
*/
 
*****************************
** Determine Dropout Group **
**                         **
**                         **
** Need to derive variable **
** dgroup = last visit     **
** included for patients   **
** dropout group           **
*****************************;


proc sort data=_temp;
by subjid descending visit;
run;

data _temp;
 set _temp;
by subjid descending visit;
if first.subjid then dgroup=visit;  ***FLAGS HIGHEST VISIT, ASSUMES MONOTONE;
retain dgroup;
run;

PROC PRINT N;
RUN;


*******************************
** calculate N per treatment **
** at each visit (needed for **
** table later)              **
*******************************;

proc sql noprint;
     create table count1 as
            select visit as visit, treatment as trt, count(subjid) as ncount from _temp
            group by visit, treatment;
    quit;


 
proc sort data=_temp;
by subjid visit;
run;




PROC SQL NOPRINT;
SELECT COMPRESS(PUT(MIN(VISIT), BEST4.)) INTO :vismin FROM _temp;
SELECT COMPRESS(PUT(MAX(VISIT), BEST4.)) INTO :vismax FROM _temp;
QUIT;





Data _chk;
set _temp;
if last.subjid;
by subjid visit;
run;












proc sort nodupkey data=_temp(keep=&modvars dgroup) out=fullset;  ***ONE OBSERVATION PER SUBJECT;
by &modvars dgroup;
run;


PROC PRINT N;
RUN;



data fullset;  ***CREATED PADDED DATA SET;
 set fullset;
 by &modvars;
 do visit = &vismin to &vismax;
  output;
 end;
 run;

 PROC PRINT N;
 RUN;

proc sort data=_temp;
by &modvars visit;
run;

data all;
 merge  fullset _temp;
 by &modvars visit;
run;


 
****DEFINE SUMMARY STATS AS GLOBAL MACRO VARIABLES;
PROC SQL NOPRINT;

SELECT COUNT(UNIQUE treatment) INTO :numtrts FROM all;

SELECT COUNT(UNIQUE DGROUP) INTO :grpnum FROM all;

SELECT UNIQUE max(VISIT) INTO :grpvals separated by ',' FROM all

group by dgroup;

QUIT;


%put _user_;



*-----------end of data preparation----*;


















***********************************************************************************************************************************;
******  PART 3:   SUPPORT MACROS                                                                                               ****;
***********************************************************************************************************************************;

*************************************
** MACRO: MATCHMERGE               **
**                                 **
** Creates Dataset with each item  **
** from one set matched with each  **
** the unique values of one        **
** variable in another dataset     **
**                                 **
** inset1 = First Dataset          **
** inset2 = Dataset with variable  **
**          that will be used      **
** var2 = Variable which unique    **
**        values will be matched   **
**        and merged with inset1   **
** outset1 = output dataset        **
**                         **
** Note: This Macro used to create **
**       contrasts for final model **
**       Is used within later      **
**       macro ANALYZE             **
*************************************;

%macro matchmerge(inset1=, inset2=, var2=,outset1=);

data temp;
   set &inset1;
   tag=1;
run;

data temp;
  set temp;
  by tag;
  if first.tag then num=1;
  else num=num+1;
  retain num;  
run;

data _NULL_;   
  set temp;
  by tag num;
  if last.tag;
  call symput("numvals", compress(put(num,4.)) );
run;

proc sort data=&inset2(KEEP=&VAR2) out=temp2 nodupkey ; 
  by &var2;
run;


data temp2;     
 set temp2;
 keep &var2;
run;

data temp2;
 set temp2;
 do num = 1 to &numvals;
  output;
 end;
run;

proc sort data=temp2;
by num;
run;


data &outset1;
 merge temp temp2;
 by num;  
 drop num tag;
run;

%mend matchmerge;








******************************************************************
** MACRO FIRSTANL                                               **
**                                              **
** Macro to run analyses on subgroups of patients to get needed **
** values for imputing missing values later on.  The subgroups  **
** of patients used depend on the identifiability assumptions   **
** being made and the identifiability "type" being used.        **
**                                                 **
** Possible Constraint values: CCMV or NCMV                     **
**                                                              **
** CCMV --> Base later modeling on distribution derived from    **
**          completing patients (last dropout group).           **
**                                              **
** NCMV --> Based later modeling on distributions derived from  **
**          models fit separately on each dropout group. Will   **
**          model each missing visit later using data from      **
**          "closest" discontinuation group                     **
**                                                    **
** Possible Type values: NFMV or ALL                            **
**                                                              **
** NFMV --> Later modeling of missing data will be done using   **
**          distributions derived only from data up to missing  **
**          visit (and subject to above constraint)             **
**                                                              **
** ALL  --> Later modeling of msising data will be done using   **
**          distributions derived from all data (subject to     **
**          above constraint)                                   **
******************************************************************;

%macro firstanl(constraint = , type =, yvar=, model= , classvars=);

%if %UPCASE(&type) = NFMV %then %do;

   %if %UPCASE(&constraint) = CCMV %then %do;

      %do J = &vismin +1 %to &vismax;

      ODS Listing close;
      proc mixed data = all asycov;
      where dgroup=&vismax and visit le &J; 
        class &classvars subjid;
       model &yvar = &model /ddfm = kenwardroger s covb;
      repeated visit /sub = subjid type = un r;
      ods output covparms=covtemp&J; *<-- Output variance/covariance matrix for the  patient observations;
      ods output SolutionF=parmvals&J; *<-- Output Parameter Estimates from complete case analysis;
      ods output asycov=covvar&J;*<-- Output variance/covariance matrix for the estimated variance parameters;
      ods output covb=parmcov&J; *<-- Output variance/covariance matrix for the estimated model parameters;
      run;

      ODS Listing;
      run;


      %end;
   %end;

   %if %UPCASE(&constraint) = NCMV %then %do;

      %do J = &vismin +1 %to &vismax;

      data _temp; 
        set all;
        where dgroup ge &J;
        hash=1;
      run;

      proc sort data=_temp;
      by hash dgroup;
      run;

      data _temp; 
       set _temp;
       by hash dgroup;
         if first.hash then closest=dgroup;   
            if dgroup ne closest then delete;
         retain closest;
      run;

      ODS Listing close;
      proc mixed data = _temp asycov;
      where visit le &J; 
        class &classvars subjid;
       model &yvar = &model /ddfm = kenwardroger s covb;
      repeated visit /sub = subjid type = un r;
      ods output covparms=covtemp&J; *<-- Output variance/covariance matrix for the  patient observations;
      ods output SolutionF=parmvals&J; *<-- Output Parameter Estimates from analysis;
      ods output asycov=covvar&J;*<-- Output variance/covariance matrix for the estimated variance parameters;
      ods output covb=parmcov&J; *<-- Output variance/covariance matrix for the estimated model parameters;
      run;

      ODS Listing;
      run;



      %end;
   %end;
   %end;

%else %do;

   %if %UPCASE(&constraint) = CCMV %then %do;

      ODS Listing close;
      proc mixed data = all asycov;
      where dgroup=&vismax; 
        class &classvars subjid;
       model &yvar = &model /ddfm = kenwardroger s covb;
      repeated visit /sub = subjid type = un r;
      ods output covparms=covtemp&vismax; *<-- Output variance/covariance matrix for the  patient observations;
      ods output SolutionF=parmvals&vismax; *<-- Output Parameter Estimates from complete case analysis;
      ods output asycov=covvar&vismax;*<-- Output variance/covariance matrix for the estimated variance parameters;
      ods output covb=parmcov&vismax;*<-- Output variance/covariance matrix for the estimated model parameters;
      run;

      ODS Listing;
      run;

   %end;

   %if %UPCASE(&constraint) = NCMV %then %do;

      %do J = &vismin +1 %to &vismax;


      ODS Listing close;
      proc mixed data = all asycov;
      where dgroup = &J and &yvar ne .; 
        class &classvars subjid;
       model &yvar = &model /ddfm = kenwardroger s covb;
      repeated visit /sub = subjid type = un r;
      ods output covparms=covtemp&J; *<-- Output variance/covariance matrix for the  patient observations;
      ods output asycov=covvar&J;*<-- Output variance/covariance matrix for the estimated variance parameters;
      ods output covb=parmcov&J;*<-- Output variance/covariance matrix for the estimated model parameters;
      ods output SolutionF=parmvals&J; *<-- Output Parameter Estimates from analysis;
      run;

      ODS Listing;
      run;
      %end;




  %end;
%end;

%mend firstanl;







**************************************************
** MACRO: GENCOV                                **
**                                              **
** Macro to generate a random draw of the       **
** subject covariance matrix based on the       **
** fitted asymptotic covariance matrix along    **
** with the covariance of the fitted parameters **
** as fit within proc mixed earlier.  The output**
** from this macro will be a generated          **
** covariance matrix.                           **
**                                              **
** incovmean = fitted covariance parameters     **
** incovvar = covariance matrix for fitted      **
**            parameters                        **
** outcovset = output set for generated         **
**             covariance                       **
** visitval = Highest visit number for matrix   **
**************************************************;


%macro gencov(incovmean=,incovvar=,outcovset=,visitval=);

Data _cov_check1;
  set &incovvar;
  drop row covparm;
run;

data _cov_check2;
  set &incovmean;
  drop covparm subject;
run;

***********************************************
** Create loop to continue selecting random  **
** covariance matrices until one is selected **
** that is a proper covariance matrix        **
** (i.e. is symmetric and positive definite) **
**                                           **
** If a root of the matrix exists, the matrix**
** can be a proper covariance matrix         **
***********************************************;
 
%let status=0;

%do %until(&status = 1);

proc IML;

   **Pick next available seed value from generated seed list and set as seed for randomization**;
   
   use seedvals; read all into seeds;

   call randseed(seeds[1]);

   tseed=seeds[2:nrow(seeds),]; 

   create seedvals2 from tseed;
   append from tseed;


   ** Read in mean and variance values **;

   use _cov_check1; read all into varmatrix;
   use _cov_check2; read all into varmean;
   checkme=0;

   ** Generate Random Variance Parameters **;

      T = root( varmatrix); 
   Z=j(nrow(varmatrix),1,.); 
    call randgen(Z,'NORMAL'); 
    genvals = T`*Z +varmean; 

   ** Put parameters in proper symmetric matrix form **;

   indnum=0;
   zmatrix=j(&visitval - &vismin + 1,&visitval - &vismin + 1,.); 

   do i=1 to &visitval - &vismin +1;
    do j=1 to i;
      indnum=indnum+1;
      zmatrix[i,j] = genvals[indnum];
      zmatrix[j,i] = genvals[indnum];
    end;
    end;

   ** Check if root exists, if it does, variable checkme > 0**;


   check2 = min(eigval(zmatrix));   

   if check2> 0 then checkme=1;
   
create &outcovset from zmatrix;
append from zmatrix;

create _cov_checkset from checkme;
append from checkme;
quit;

run;

**************************
** Replace the old seed **
** list with the new    **
** list that removed the**
** seed values already  **
** used                 **
**************************;

data seedvals;
 set seedvals2;
 run;

 data _null_;   
  set _cov_checkset;
  call symput("status", compress(put(col1, 8.)));
 run;


%end;


%mend gencov;














**************************************************
** MACRO: GENPARMS                              **
**                                              **
** Macro to generate a random draw of the       **
** model parameter values based on the mean     **
** fitted model parameter values along with its **
** covariance.  Randomly selected parameter     **
** values put into same format as seen from     **
** proc mixed.                                  **
**                                              **
** inparm = mean model parameters from fitted   **
**          model                               **
** incov = covariance matrix for fitted         **
**            parameters                        **
** outparm = output set for generated parameters**
**************************************************;


%macro genparms(inparm=,incov=,outparm=);

*********************
Remove Zero estimates 
that are default     
values               
*********************;

Data _parm_check1;
  set &incov;
  if abs(col1) > .000001;
  drop row effect &classvars;
run;

data _parm_check2;
  set &inparm;
  if stderr ne .;
  keep estimate;
run;

data _parm_check3;   
  set &incov;
 if abs(col1) > .000001;
  keep row effect &classvars;
run;


proc IML;

   **Pick next available seed value from generated seed list and set as seed for randomization**;
   
   use seedvals; read all into seeds;

   call randseed(seeds[1]);

   tseed=seeds[2:nrow(seeds),]; 

   create seedvals2 from tseed;
   append from tseed;

   ** Read in mean and variance values **;

   use _parm_check1; read all into varparms; 
   use _parm_check2; read all into parmmean; 

   ** Get rid of zero variance columns **;

   vmatrix = j(nrow(parmmean),nrow(parmmean),.);

   do j= 1 to nrow(parmmean);
      k=1;
   do i = 1 to ncol(varparms);
    
    if abs(varparms[j,i]) > .000001 then do;
         vmatrix[j,k]=varparms[j,i];
        k=k+1;
    end;
    end;
   end;
           
   ** Generate parameter draw **;

    T = root(vmatrix); 
   Z=j(nrow(parmmean),1,.); 
    call randgen(Z,'NORMAL'); 

   LL= Z`*T;
    genout = T`*Z + parmmean;

create _parm_check4 from genout;
append from genout;

quit;

run;

**************************
** Replace the old seed **
** list with the new    **
** list that removed the**
** seed values already  **
** used                 **
**************************;

data seedvals;
 set seedvals2;
 run;

**************************
** Put generated values **
** into same form as    **
** seen in proc mixed   **
**************************;

data _parm_check5;
 merge _parm_check3 _parm_check4;
 rename col1=estimate;
run;

data _parm_check6; 
  set &incov;
  keep row effect &classvars;
run;



 data &outparm;
 merge _parm_check5 _parm_check6;
 by row;
 if estimate=. then estimate=0;
 run;

%mend genparms;

**************************************************
** MACRO: GENVALS                               **
**                                              **
** Macro to generate imputations for missing    **
** values at a given visit given all previous   **
** visits                                       **
**                                              **
** dataset = Input dataset                      **
** firstvis = Visit for which missing values    **
**            will be generated                 **
** basevis = First treatment visit              **
**************************************************;


%macro genvals(dataset=,firstvis=, basevis=, yvar=);

data _val_missv; 
 set &dataset;
 if &yvar=.;
run;

proc sort data=_val_missv;
by subjid visit;
run;

data _val_missv;
 set _val_missv;
 by subjid visit;
 if first.subjid;
 firstmiss=visit;
 keep subjid firstmiss;
run;

proc sort data=&dataset out=_val_temp;
by subjid;
run;

data &dataset;     
 merge _val_missv _val_temp;
 by subjid;
run;

data _val_working;
 set &dataset;
 if firstmiss=&firstvis and visit le &firstvis;
 tvis=visit;
 tbase=base;
run;

***************************
** Get design matrix for **
** observations to get   **
** needed predicted      **
** values                **
***************************;
proc logistic data=_val_working outdesign=_val_workx outdesignonly;
      class &classvars / param=glm ; 
      model tbase = subjid tvis &model;
      run;

data _val_workx1;
 set _val_workx;
 visit=tvis;
 keep subjid visit;
run;

data _val_workx2;
 set _val_workx;
 drop subjid tvis tbase;
run;




proc IML;

use parms&firstvis; read all var{Estimate} into betahat; *<-- read in earlier parameter estimates;


use _val_workx2; read all into xmatrix; *<-- read in completed design matrix;
use _val_workx1; read all into xnames;  *<-- create matrix of subject and visit info 
                                          needed for later merging;

predict = xmatrix*betahat; *<-- Get predicted values for each subject at each visit;

predout = xnames || predict; *<-- Append corresponding subjid and visid values to predictions;

create _val_predicted from predout; *<-- Create dataset of predicted values;
append from predout;


quit;


data _val_predicted;
 set _val_predicted;
 rename col1=subjid col2=visit col3=predval;
 run;

data _val_predcheck&firstvis;
set _val_predicted;
run;


proc sort data=_val_predicted; 
by subjid visit;
run;

proc sort data=_val_working;
by subjid visit;
run;

data _val_working_new;
 merge _val_working _val_predicted;
 by subjid visit;

 if &yvar>. and predval>. then  diffpred=&yvar - predval;
run;

data _val_working_new1;
set _val_working_new;
if diffpred ne .;
run;




proc sort data=_val_working_new1;
by subjid;
run;


*********************************************
** Transpose to create 1 line per patient. **
**                                         **
** this makes later matrix calculations    **
** easier to implement.                    **
*********************************************;

proc transpose data=_val_working_new1 prefix=dif out=_val_comp;
by subjid;
var diffpred;
run;

data _val_patval; 
 set _val_comp;
 keep subjid;
run;

data _val_comp;
 set _val_comp;
 drop subjid;
run;

data _val_miss;
 set _val_working_new;
 if diffpred = .; 
 keep predval;
run;

data _val_cov;
 set covm&firstvis;
 *drop row index;
run;



proc IML;

   **Pick next available seed value from generated seed list and set as seed for randomization**;
   
   use seedvals; read all into seeds;

   call randseed(seeds[1]);

   **Remove the seed used so repeat of this part of process will use a different seed**;

   tseed=seeds[2:nrow(seeds),];

   create seedvals2 from tseed;
   append from tseed;    

   ** Create matrices that will get components of variance matrices needed later **;
   zero = {0};
    one = {1};
   top = I(&firstvis - &basevis);
   bottom = J(1,&firstvis - &basevis,0);

   leftmatrix=top // bottom;
   rightmatrix = bottom` // one;

   
   ** read in subjid vector, difference between observed and predicted matrix, **
   ** and predicted matrix as generated above for placebo missing observations.**;

   use _val_patval; read all into pts;
   use _val_comp; read all into cp;
   use _val_miss; read all into ms;

   ** read in needed variance and covariance matrix and derive needed submatrices **
   ** of observed values**;

   use _val_cov; read all into cvm;

   qmat = leftmatrix` * cvm * leftmatrix;
   vmat = rightmatrix` * cvm * rightmatrix;
   pmat = rightmatrix` * cvm * leftmatrix;


   ** Calculate covariance matrix for missing observations given the **
   ** observed values (used for Placebo only)                        **;

   varP = vmat - pmat * INV(qmat) * pmat`;

   ** Calculate mean vector for missing observations given the **
   ** observed values (used for Placebo only)                  **;

   tval = pmat * INV(qmat);
   mout= ms` + tval * cp`;

   T = root( varP ); 
   Z=j(nrow(pts),1,.);
    call randgen(Z,'NORMAL');
    genvals = Z*T + mout`;

   ** Append subject IDs to the corresponding imputed observations **;
 
   _val_outgen = pts || genvals;

    ** read in subjid vector and predicted matrix as generated above **
   ** for active treatment missing observations.                    **;

   create _val_out_gen from _val_outgen;
   append from _val_outgen;

quit;

run;

**************************
** Replace the old seed **
** list with the new    **
** list that removed the**
** seed values already  **
** used                 **
**************************;

data seedvals;
 set seedvals2;
 run;


data _val_miss;
 set _val_working_new;
 if diffpred = .;
run;

data _val_out_gen2;
 set _val_out_gen;
 rename col1=subjid col2=mival;
 run;



 data _val_miss2;
  merge _val_miss _val_out_gen2;
  by subjid;
  keep subjid visit mival;
run;

data &dataset;
 merge &dataset _val_miss2;
 by subjid visit;
 if mival ne . then &yvar=mival;
 drop firstmiss mival;
run;

data _val_checkset&firstvis;  
 set &dataset;
 run;

%mend genvals;





**************************************************
** MACRO: MEGACALL                              **
**                                              **
** Macro to generate full imputations through   **
** repetive call of GENCOV, GENPARM, and GENVALS**
**                                              **
** numimp = Number of imputations to generate   **
**                                              **
** firstv = first visit  of treatment           **
**                                              **
** lastvis = last treatment visit               **
**************************************************;




%macro megacall(numimp=, Itype=, Iconstraint=, yvar=, firstv=&vismin,lastvis=&vismax);

options minoperator; ** <-- required option to allow the use of the "in" operator in macro IF statements;

data midatasets;
 set _NULL_;
run;

%do K = 1 %to &numimp;

** If using complete data based on CCMV or NFMV constraints, need to generate random covariance matrix and parameter 
   value draws from models fitted.  Then need to subset the values drawn to get the appropriate matrices and parameter
   values for estimating a given visit when all previous visits are known. ;

%if &Itype ne NFMV %then %do;
   %if &Iconstraint = CCMV %then %do;
   %gencov(incovmean=covtemp&vismax,incovvar=covvar&vismax,outcovset=covm&vismax,visitval=&vismax);
   %genparms(inparm=parmvals&vismax,incov=parmcov&vismax,outparm=parms&vismax);

   %do M = &vismin %to &vismax - 1;
      data parms&M;
       set parms&vismax;
       if visit gt &M then delete;
      run;
         
      data covm&M;
       set covm&vismax;
       tag=&vismin;
      run;

         
      data covm&M;
       set covm&M;
       by tag;
       if first.tag then row=tag;
       else row=row+1;
       retain row;
      run;

      data covm&M;
       set covm&M;
       if row le &M;
       drop row tag;
      run;


      %do L = &M -&vismin + 2 %to &vismax -&vismin + 1;

      data covm&M;
       set covm&M;
       drop col&L;
      run;

      %end;
   %end;

   %end;

   %if &Iconstraint = NCMV %then %do;

   %do M = &vismax %to &vismin +1 %by -1;

   %if &M in &grpvals %then %do;
   %gencov(incovmean=covtemp&M,incovvar=covvar&M,outcovset=covm&M,visitval=&M);
   %genparms(inparm=parmvals&M,incov=parmcov&M,outparm=parms&M);

   data tempcov;
    set covm&M;
    numcol=&M - &vismin +1;
    run;

    data tempparms;
    set parms&M;
    run;

    %end;
    %else %do;

     data parms&M;
      set tempparms;
       if visit gt &M then delete;
     run;

    data covm&M;
     set tempcov;
     run;

    data covm&M;
     set covm&M;
     by numcol;
     if first.numcol then row=&vismin;
     else row=row+1;
     retain row;
    run;

    data covm&M;
     set covm&M;
     if row le &M;
     drop row numcol;
    run;

   %do L = &M -&vismin + 2 %to &vismax -&vismin + 1;

     data covm&M;
     set covm&M;
    drop col&L;
    run;

   %end;


     %end;
   %end;

   %end;

%end;


data tempall; 
 set all;
run;

%do J = &firstv + 1 %to &lastvis;


**** If NFMV restriction, draw covariance matrices and parameter estimates right before use;

%if &Itype = NFMV %then %do;

   %gencov(incovmean=covtemp&J,incovvar=covvar&J,outcovset=covm&J,visitval=&J);

   %genparms(inparm=parmvals&J,incov=parmcov&J,outparm=parms&J);

%end;

%genvals(dataset=tempall, yvar=&yvar, firstvis=&J, basevis=&firstv); *--QL: add yvar--*;

%end;

data tempall;
 set tempall;
 imputation = &K;
run;


data midatasets;
 set midatasets tempall;
run;

%end;

%mend megacall;




*****************************************
** MACRO ANALYZE                       **
**                                     **
** Macro performs analysis on each     **
** imputed dataset and then computes an**
** overall imputed parameter estimate  **
** and parameter variance/covariance   **
** across the imputations.             **
**                                     **
** These values are then used to       **
** calculate LSMEAN values and         **
** confidence intervals at each study  **
** visit.                              **
**                                     **
** For this macro, it is assumed that  **
** the final model used for analysis   **
** has terms for dropout pattern.      **
*****************************************;
%let classvars2=%str();


%macro analyze(numimp=, yvar=, model2=, classvars2=,othervars=);

*-----get numclass2----*;

data _nul_;

char="&classvars2";

do i=1 to 100;
 word=scan(char, i,''); if word ne "" then do;
call symput("numclass2", compress(put(i, 4.))); output;
                                           end;
end;

run;




%do I = 1 %to &numimp;

ODS Listing Close;
proc mixed data = midatasets;
where Imputation =&I;
   class &classvars2 subjid;
   model &yvar = &model2/ddfm = kenwardroger s covb;
   repeated visit /sub = subjid type = un;
  ods output covb = cov&I; *<-- Output variance/covariance estimates for parameter estimates;
  ods output SolutionF=parmfin&I;*<-- Output Parameter Estimates from analysis on Ith imputation;
run;
ODS Listing;

data cov&I;
  set cov&I;
  drop row effect &classvars2;
  run;



%end;


** Calculate mean of continuous parameters for use within LSMean calculations;
proc means data=midatasets noprint mean;
var &othervars ;
output out=meanvals;
run;

data meanvals;
 set meanvals;
   if _Stat_="MEAN";
   tag=1;
   keep tag &othervars;
run;


** Calculate the frequency of each dropout group.  Will use this to weight estimates;

proc freq noprint data=midatasets;
  table dgroup / out=freqs;
run;

data freqs;
 set freqs;
 freq=percent/100;
 keep dgroup freq;
run;

** Create a unique set of every possible combination of classification variable levels.;

proc sort nodupkey data=midatasets out=contrasts;
by %scan(&classvars2,1);
run;

data contrasts;
 set contrasts;
keep %scan(&classvars2,1);
run;

%do CN = 2 %to &numclass2;
  
%matchmerge(inset1=contrasts, inset2=midatasets, var2=%scan(&classvars2,&CN),outset1=contrasts);

%end;

proc sort data=contrasts;
by dgroup;
run;

** Merge in mean values and frequencies calculated earlier;

data contrasts;
 merge contrasts freqs;
 by dgroup;
 tag=1;
run;

data contrasts;
 merge contrasts meanvals;
 by tag;
 run;


data contrasts;
 set contrasts;
 tvis=visit;
 ttreat=treatment;
 keep freq tvis ttreat &classvars2 &othervars;
run;

** find design matrix corresponding to all combinations of factors.  keep a separate variable with treatment and visit
   information to select the appropriate contrasts;

proc logistic data=contrasts outdesign=contra outdesignonly;
      class &classvars2 / param=glm ; 
      model freq = tvis ttreat &model2;
      run;

** create separate datasets for each contrast matrix and cooresponding frequency vectors ;

%do TV = &vismin %to &vismax + 1;
  %do TT = 1 %to &numtrts;

   %if &TV le &vismax %then %do;
  data V&TV.T&TT;
   set contra;
   if tvis=&tv and ttreat = &tt;
   drop freq tvis ttreat;
  run;

  data FV&TV.T&TT;
   set contra;
   if tvis=&tv and ttreat = &tt;
   keep freq;
  run;

    %end;

   %else %do;

  data V&TV.T&TT;
   set contra;
   if ttreat = &tt;
   drop freq tvis ttreat;
  run;

  data FV&TV.T&TT;
   set contra;
   if ttreat = &tt;
   keep freq;
  run;
    %end;

  %end;
%end;

   

** calculate estimates for each imputation and then combine to get overall parameter and variance matrix.
   Use these to calculate by visit estimates;

proc iml;

%do J = 1 %to &numimp;

use parmfin&J; read all var{Estimate} into betahat&J;
use cov&J; read all into vhat&J;

%if &J=1 %then %do;
  totparm=betahat1;
  totvhat=vhat1;
%end;
%else %do;
  totparm = totparm + betahat&J;
  totvhat = totvhat + vhat&J;
%end;

%end;

aveparm=totparm/&numimp; *<-- average parameter estimate across imputations;
what = totvhat/ &numimp; *<-- average parameter covariance matrix across imputations;


%do K = 1 %to &numimp;

bvar&K = betahat&K - aveparm;

%if &K=1 %then %do;
  totbvar= bvar1*bvar1`;
%end;
%else %do;
  totbvar = totbvar + bvar&K * bvar&K`;
%end;
%end;

bhat = totbvar/(&numimp - 1); *<-- estimated covariance matrix between imputations;

That = what + (1 + 1/(&numimp))*bhat; *<-- estimated variance/covariance for parameter estimates;

** calculate estimates at each visit;

%do K = &vismin %to &vismax + 1;
  %do M = 1 %to &numtrts;

use V&K.T&M; read all into T&M.V&K;
use FV&K.T&M; read all into FT&M.V&K;

T&M.V&K = FT&M.V&K`*T&M.V&K;
correct = &grpnum / nrow(FT&M.V&K);
T&M.V&K = correct*T&M.V&K;

%end;

%end;

%do K = &vismin %to &vismax + 1;
  %do M = 1 %to &numtrts;

   lm = T&M.V&K * aveparm;
   var = T&M.V&K * that * T&M.V&K`;
   trt ={&M};
   vis = {&K};

   all= lm ||var || trt || vis;

   %if &K = &vismin and &M = 1 %then %do;

    allout = all;
   %end;
   %else %do;
    allout = allout // all;
   %end;

   %if &M ne &numtrts %then %do;

         %do H = &M+1 %to &numtrts;

         difv = T&M.V&K - T&H.V&K;
         lm = difv * aveparm;
         var = difv * that * difv`;
         trt ={&M};
         trt2={&H};
         vis = {&K};

         diff= lm || var || trt || trt2 || vis;
            %if &K = &vismin and &M = 1 &H = &M+1 %then %do;
                diffout = diff;
            %end;
            %else %do;
                diffout = diffout // diff;
            %end;

         %end;

   %end;



%end;
%end;

create all_means from allout;
append from allout;

create all_diffs from diffout;
append from diffout;


quit;
run; 

%mend analyze;






***********************************************************************************************************************************;
******  PART 4:    PATTERNMIXTURE MACRO                                                                                        ****;
***********************************************************************************************************************************;




%MACRO patternmixture(analyset=, Iconstraint=, Itype=,seedgen=, numberimputations=,
                      YVAR=, MODEL=%STR( ),modvars=%STR( ), classvars=%STR( ),  id=, 
                      MODEL2=%STR( ),classvars2=%STR( ), othervars=%STR( ), 
                 TITLE=%STR( ), FOOTNOTE=%STR( ),
                      DEBUG=0);

****************************
** Create dataset of seed **
** values to support later**
** imputation.            **
****************************;



data seedvals;
call streaminit(&seedgen);
 do i = 1 to 1000;
    x1=rand('uniform');
    x1= int(100000000*x1);
   output;
 end;
 keep x1;
run;
 


*************************
*Run Needed Models      *;
*************************;

%firstanl(constraint = &Iconstraint, type = &Itype, yvar=&yvar, model= &model, classvars=&classvars);



*************************************
*Generate imputations               *;
*************************************;

%megacall(numimp=&numberimputations, Iconstraint=&Iconstraint, Itype=&Itype, yvar=&yvar, firstv=&vismin,lastvis=&vismax);


** Perform analyses;

%analyze(numimp=&numberimputations, yvar=&yvar, model2=&model2, classvars2=&classvars2,othervars=&othervars);



*****************************
** Order and output Report **
*****************************;

data all_diffs2;
 set all_diffs;
 stderrdif=sqrt(col2);
rename col1=lsmeandiff col3=trtr col4=trt col5=visit;
drop col2;
run;

data all_means2;
 set all_means;
 stderrtrt=sqrt(col2);
rename col1=trtlsmean col3=trt col4=visit;
drop col2;
run;

proc sort data=all_diffs2;
by visit trt;
run;

proc sort data=all_means2;
by visit trt;
run;


data out_diff2;
 merge all_diffs2 all_means2;
 by visit trt;
run;

proc sort data=out_diff2;
by visit trt;
run;

data out_diff2;
 set out_diff2;
 IF stderrtrt>. THEN LCLT = trtlsmean - 1.96 * stderrtrt;
 IF stderrtrt>. THEN UCLT = trtlsmean + 1.96 * stderrtrt;
 IF stderrdif>. THEN LCL = lsmeandiff - 1.96 * stderrdif;
 IF stderrdif>. THEN UCL = lsmeandiff + 1.96 * stderrdif;
 IF stderrdif>0 THEN  diffpvalue = 2-2*probnorm(abs(lsmeandiff/stderrdif));
 run;


 data count2;
  set count1;
  if visit=&vismin;
  visit=&vismax + 1;
  run;

  data count1;
   set count1 count2;
  run;

  proc sort data=count1;
   by visit trt;
  run;


data out;
     merge out_diff2 count1;
     by visit trt;
run;

ods listing;


 title1 "&title";
 title2 "Using &Iconstraint identifiability constraint";
 title3 "By visit LSMEANS using Model with Effects for Dropout Pattern";




 proc report data=out headline nocenter nowindows missing split = "*";
 column  visit trt ncount trtlsmean stderrtrt lsmeandiff stderrdif LCL UCL diffpvalue;
 
 define visit/'Visit*(Week)' left spacing = 1 width = 9 order order=data f=visit.;
 define trt/'Treatment' left spacing = 1 width = 15 f=trt.;
 define ncount/'N' left spacing = 1 width = 3 f=6.;
 define trtlsmean/'LSMEAN*Change' center spacing = 1 width = 10 f=6.2;
 define stderrtrt/'SE' center spacing = 1 width = 8 f=6.2;
 define lsmeandiff/'LSmean*Difference' center spacing = 1 width = 15 f=6.2;
 define stderrdif/'SE' center spacing = 1 width = 8 f=6.2;
 define LCL/'Lower*CL' center spacing = 1 width = 10 f=6.2;
 define UCL/'Upper*CL' center spacing = 1 width = 10 f=6.2;
 define diffpvalue/'P-value' center spacing=1 width =15 f=5.4;

 break after visit/skip;
 compute after _page_;
     line @2 131*"-"; 
     line @2 "&Itype observations used subject to identifiable constraint";
    line @2 "Model:&model2";
   endcomp;
run;

%mend patternmixture;









