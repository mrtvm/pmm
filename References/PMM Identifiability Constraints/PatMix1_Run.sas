/* Code to run the PatMix1.sas macro */
/* Change the macro variables below to match your machine */
%let MacroPath = C:\Users\sasrdw\Documents\Missing Data\MBSW\PMM Identifiability Constraints;
%let DataPath = C:\Users\sasrdw\Documents\Missing Data\MBSW;
%let DataName = chapter15_example;

libname DATALIB "&DataPath"; 

****************************
** Study Specific Formats **
** and Output Titles      **
****************************;

proc format;
	value visit
		4 = 'Visit 4'
		5 = 'Visit 5'
		6 = 'Visit 6'
		7 = 'Visit 7'           
		8 = 'Overall'
	;
   value trt 
		1 = "Drug"
		2 = "Placebo"
	;
run;

%include "&MacroPath./PatMix1.sas" / nosource;

* complete case;
%patternmixture(
	analyset=datalib.&DataName,
	Iconstraint=CCMV, 
	Itype=NFMV,
	seedgen=8857954, 
	numberimputations=5,
   YVAR=%STR(CHANGE),
   MODEL=%STR(base treatment visit base*treatment base*visit treatment*visit base*treatment*visit),
   modvars=%STR(subjid treatment base), 
	classvars=%STR(treatment visit),  id=, 
	MODEL2=%STR(base inv dgroup treatment visit dgroup*treatment base*treatment base*visit treatment*visit dgroup*treatment*visit base*treatment*visit),
	classvars2=%STR(dgroup INV treatment visit), 
	othervars=%STR(base), 
	TITLE=%STR(CCMV Pattern Mixture Model Analysis), 
	FOOTNOTE=%STR( )
);

* nearest case;
%patternmixture(
	analyset=datalib.&DataName,
	Iconstraint=NCMV, 
	Itype=NFMV,
	seedgen=8857954, 
	numberimputations=5,
   YVAR=%STR(CHANGE),
   MODEL=%STR(base treatment visit base*treatment base*visit treatment*visit base*treatment*visit),
   modvars=%STR(subjid treatment base), 
	classvars=%STR(treatment visit),  id=, 
	MODEL2=%STR(base inv dgroup treatment visit dgroup*treatment base*treatment base*visit treatment*visit dgroup*treatment*visit base*treatment*visit),
	classvars2=%STR(dgroup INV treatment visit), 
	othervars=%STR(base), 
	TITLE=%STR(NCMV Pattern Mixture Model Analysis), 
	FOOTNOTE=%STR( )
);




